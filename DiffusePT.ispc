// ======================================================================== //
// Copyright 2009-2019 Intel Corporation                                    //
//                                                                          //
// Licensed under the Apache License, Version 2.0 (the "License");          //
// you may not use this file except in compliance with the License.         //
// You may obtain a copy of the License at                                  //
//                                                                          //
//     http://www.apache.org/licenses/LICENSE-2.0                           //
//                                                                          //
// Unless required by applicable law or agreed to in writing, software      //
// distributed under the License is distributed on an "AS IS" BASIS,        //
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. //
// See the License for the specific language governing permissions and      //
// limitations under the License.                                           //
// ======================================================================== //

#include "math/random.ih"
#include "math/sampling.ih"
#include "render/Renderer.ih"

#include "DiffusePTMaterial.ih"

// ISPC renderer structure ////////////////////////////////////////////////////

struct DiffusePT
{
  Renderer super;
  int maxDepth;
  float R;
};

// Helper functions ///////////////////////////////////////////////////////////

inline vec4f getSurfaceColor(const DifferentialGeometry &dg)
{
  const DiffusePTMaterial *mat = (const DiffusePTMaterial *)dg.material;

  vec3f surfaceColor = make_vec3f(1.f);
  float opacity      = 1.f;
  if (mat) {
    foreach_unique(m in mat)
    {
      surfaceColor = m->Kd;
      if (m->map_Kd) {
        vec4f Kd_from_map = get4f(m->map_Kd, dg);
        surfaceColor      = surfaceColor * make_vec3f(Kd_from_map);
      }
      opacity = m->d;
    }
  }

  return make_vec4f(surfaceColor * make_vec3f(dg.color), opacity);
}

inline void computeNewRay(Ray &ray,
                          const uniform int fbWidth,
                          const uniform int depth,
                          const vec3i &sampleID,
                          const varying DifferentialGeometry &dg)
{
  RandomTEA rng_state;
  varying RandomTEA *const uniform rng = &rng_state;
  RandomTEA__Constructor(rng, 0x290374, (fbWidth * sampleID.y) + sampleID.x);
  const vec2f rot = RandomTEA__getFloats(rng);

  const linear3f localToWorld = frame(dg.Ns);

  const vec2f halton       = HaltonSequence_get2D(sampleID.z + depth);
  const vec2f r            = CranleyPattersonRotation(halton, rot);
  const vec3f local_ao_dir = cosineSampleHemisphere(r);
  const vec3f ao_dir       = localToWorld * local_ao_dir;

  setRay(ray, dg.P, ao_dir, dg.epsilon, 1e30f);
}

// Main renderSample //////////////////////////////////////////////////////////

void DiffusePT_renderSample(Renderer *uniform _self,
                            FrameBuffer *uniform fb,
                            World *uniform world,
                            void *uniform perFrameData,
                            varying ScreenSample &sample)
{
  uniform DiffusePT *uniform self = (uniform DiffusePT * uniform) _self;

  Ray ray = sample.ray;

  vec3f outputColor = make_vec3f(self->super.bgColor);
  vec3f albedo      = outputColor;
  vec3f normal      = make_vec3f(0.f);

  uniform int depth = 0;

  float Lw = 1.f;

  while (depth < self->maxDepth) {
    traceRay(world, ray);

    if (noHit(ray))
      break;

    DifferentialGeometry dg;
    postIntersect(world,
                  dg,
                  ray,
                  DG_NG | DG_NS | DG_NORMALIZE | DG_FACEFORWARD |
                      DG_MATERIALID | DG_COLOR | DG_TEXCOORD);

    if (depth == 0) {
      outputColor = make_vec3f(getSurfaceColor(dg));
      albedo      = outputColor;
      normal      = dg.Ns;
    }

    computeNewRay(ray, fb->size.x, depth, sample.sampleID, dg);

    Lw *= 0.8f;
    depth++;
  }

  const float eyeLightIntensity = absf(dot(normal, sample.ray.dir));

  if (depth > 0)
    outputColor = outputColor * ((0.8f * Lw) + (0.2f * eyeLightIntensity));
  else
    outputColor = outputColor * Lw;

  sample.rgb    = outputColor;
  sample.albedo = albedo;
  sample.normal = normal;
  sample.alpha  = 1.f;
}

// DiffusePT C++ interface ////////////////////////////////////////////////////

export void *uniform DiffusePT_create(void *uniform cppE)
{
  uniform DiffusePT *uniform self = uniform new uniform DiffusePT;
  Renderer_Constructor(&self->super, cppE, 1);
  self->super.renderSample = DiffusePT_renderSample;
  return self;
}

export void DiffusePT_set(void *uniform _self, uniform int depth)
{
  uniform DiffusePT *uniform self = (uniform DiffusePT * uniform) _self;

  self->maxDepth = depth;
  self->R        = 0.8f;
}
